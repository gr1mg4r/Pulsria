local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local itemBan = require "necro.game.item.ItemBan"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

local ecs = require "system.game.Entities"


local BURROWED = attack.Flag.extend("BURROWED")

components.register {
	Pulsria_spellCooldownOnHit = {},
	Pulsria_bypassLuteShield = {},
}

event.entitySchemaLoadNamedEnemy.add("addFlagToMoles", {key = "mole", sequence = 1}, function (ev)
	ev.entity.stasisAttackableFlags.add = attack.Flag.mask(ev.entity.stasisAttackableFlags.add, BURROWED)
end)

event.objectDealDamage.add("bypassLuteShield", {order = "penetration", filter = "Pulsria_bypassLuteShield"}, function (ev)
	if ev.victim.luteHead then
		ev.type = damage.Flag.mask(ev.type, damage.Flag.STRENGTH_BASED)
	end
end)

event.spellcast.add("cooldown", {order = "statusEffects", filter = "Pulsria_spellCooldownOnHit"}, function (ev)
	if ev.targetCount > 0 then
		for item in ecs.entitiesWithComponents {"spellCooldownTime", "item"} do
			if item.item.holder == ev.caster.id then
				item.spellCooldownTime.remainingTurns = math.max(3 - ev.targetCount, 0)
			end
		end
		if ev.targetCount > 2 then
			voice.tryPlay(ev.caster, "voiceStairsUnlock")
		elseif ev.caster.voiceMeleeAttack then
			voice.play(ev.caster, ev.caster.voiceMeleeAttack.sounds[ev.targetCount + 2])
		end
	else
		voice.tryPlay(ev.caster, "voiceConfused")
	end
end)

commonSpell.registerSpell("SpellcastPulseLesser", {
	spellcastTargetTiles = {
		offsets = {{  1,  1 }, {  1, -1 }, { -1,  1 }, { -1, -1 }},
	},
	spellcastInflictDamage = {
		damage = 1,
		type = damage.Type.MAGIC,
		attackFlags = attack.Flag.mask(attack.Flag.CHARACTER, attack.Flag.PROVOKE, BURROWED),
	},
	Pulsria_spellCooldownOnHit = {},
	spellcastCoreSwipe = { type = "Pulsria_pulse" },
	spellcastFlyaway = { text = "Lesser Pulse" },
	friendlyName = { name = "Lesser Pulse" },
})

customEntities.extend {
	name = "SpellPulseLesser",
	template = customEntities.template.item("spell_pulse"),
	components = {
		itemCastOnUse = { spell = "Pulsria_SpellcastPulseLesser" },
		spellCooldownKills = false,
		spellCooldownTime = {},
		itemScreenFlash = false,
		voiceSpellTypeSuffix = false,
	},
}

customEntities.extend {
	name = "Pulsria",
	template = customEntities.template.player(2),
	components = {
		friendlyName = { name = "Pulsria" },
		initialInventory = {
			items = {"Bomb", "ShovelBasic", "WeaponEli", "Pulsria_SpellPulseLesser", "MiscPotion", "CharmNazar"},
		},
		bestiary = { image = "mods/Pulsria/bestiary.png" },
		Pulsria_bypassLuteShield = {},
		playableCharacterUnlockable = false,
		playableCharacterUnlockOnRunCompletion = false,
		inventoryBannedItemTypes = {
			types = { Pulsria_SpellPulseLesser = itemBan.Type.LOCK },
		},
		textCharacterSelectionMessage = {
			text = "Pulsria Mode: Pulse only.\nDie in a single hit.\nDie on missed beat!",
		},
	}
}

event.swipe.add(nil, "Pulsria_pulse", function (ev)
	ev.swipe.texture = "ext/spells/pulse_attack.png"
	ev.swipe.width = 119
	ev.swipe.height = 120
	ev.swipe.frameCount = 6
	ev.swipe.duration = 0.3
	ev.swipe.offsetX = -24
	ev.swipe.offsetY = 12
	ev.swipe.scaleX = 0.6
	ev.swipe.scaleY = 0.6
end)
